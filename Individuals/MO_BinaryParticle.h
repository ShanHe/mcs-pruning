/*
 * MO_BinaryParticle.h
 *
 *  Created on: Nov 12, 2010
 *      Author: rgreen
 */

#ifndef MO_BINARYPARTICLE_H_
#define MO_BINARYPARTICLE_H_

#include <vector>

class MO_BinaryParticle {
	public:
		MO_BinaryParticle();
		virtual ~MO_BinaryParticle();

		std::vector< int >      pos;
		std::vector< double >   vel;

		// Holds Multiple Objectives
		std::vector < double > fitness;
		std::vector < std::vector < int > >	pBest;
		std::vector < double > 			pBestValues;
};

#endif /* MO_BINARYPARTICLE_H_ */
